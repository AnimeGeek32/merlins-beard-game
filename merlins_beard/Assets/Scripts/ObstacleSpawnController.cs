﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObstacleSpawnController : MonoBehaviour {
	public List<GameObject> obstacles;
	public float minTargetTime = 1.0f;
	public float maxTargetTime = 3.0f;
	
	private float targetTime = 1.0f;

	// Use this for initialization
	void Start () {
		targetTime = Random.Range(minTargetTime, maxTargetTime);
		StartCoroutine("WaitTillSpawn", targetTime);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SpawnObstacle() {
		int selectNumber = (int)Random.Range(0, obstacles.Count);
		Instantiate(obstacles[selectNumber], transform.position, Quaternion.identity);
		targetTime = Random.Range(minTargetTime, maxTargetTime);
		StartCoroutine("WaitTillSpawn", targetTime);
	}

	IEnumerator WaitTillSpawn(float seconds) {
		yield return new WaitForSeconds(seconds);
		SpawnObstacle();
	}
}
