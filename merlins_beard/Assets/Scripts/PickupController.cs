﻿using UnityEngine;
using System.Collections;

public class PickupController : MonoBehaviour {
	public float speed = 30.0f;
	public float maxSpeed = 30.0f;
	public float playerSpeedBoost = 10.0f;
	public float deathByHeight = -100.0f;
	public AudioClip playerHitSound;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(transform.position.y <= deathByHeight) {
			Destroy (gameObject);
		}
		rigidbody2D.AddForce(new Vector2(0.0f, -speed));
		
		if(Mathf.Abs(rigidbody2D.velocity.y) > maxSpeed) {
			rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, Mathf.Sign(rigidbody2D.velocity.y) * maxSpeed);
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		if(other.gameObject.tag == "Player") {
			if(playerHitSound) {
				other.gameObject.audio.PlayOneShot(playerHitSound);
			}
			other.gameObject.GetComponent<PlayerController>().maxSpeed += playerSpeedBoost;
			GameObject.FindGameObjectWithTag("MainCamera").GetComponent<MainCameraController>().AddScore(10);
			Destroy (gameObject);
		} else if(other.gameObject.tag == "Dragon") {
			Destroy (gameObject);
		}
	}
}
