﻿using UnityEngine;
using System.Collections;

public class MainCameraController : MonoBehaviour {
	public Transform playerObject;
	public TextMesh hudScoreText;
	public int score = 0;

	private bool hasReachedGoal = false;

	// Use this for initialization
	void Start () {
		score = 0;
		// Adjust the score HUD's position according to the camera's rect
		Vector3 currentScorePosition = hudScoreText.transform.position;
		currentScorePosition.x = -((240.0f * Camera.main.aspect) / 2) + 10.0f;
		hudScoreText.transform.position = currentScorePosition;
		hasReachedGoal = false;
		playerObject = GameObject.FindGameObjectWithTag("Player").transform;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 currentPosition = transform.position;
		if(hasReachedGoal) {
			currentPosition.y += 110.0f * Time.deltaTime;
		} else {
			currentPosition.x = playerObject.position.x;
			currentPosition.y = playerObject.position.y;
		}
		transform.position = currentPosition;
	}

	public void InitiateEndingSequence() {
		hasReachedGoal = true;
	}

	public void AddScore(int points) {
		score += points;
		hudScoreText.text = "Score: " + score;
	}
}
