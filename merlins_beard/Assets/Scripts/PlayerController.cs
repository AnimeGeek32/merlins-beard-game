﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour {
	enum PlayerAnimState {
		IDLE,
		CLIMB
	};

	public float speed = 60.0f;
	public float maxSpeed = 60.0f;
	public List<GameObject> hairStrands;
	public int prevHairStrandIndex = 1;
	public int currentHairStrandIndex = 1;

	private bool canMove = true;
	private bool isDying = false;
	private bool hasReachedGoal = false;
	private bool switchingHairStrand = false;
	private float hairStrandSwitchTime = 0.0f;
	private bool gotHitByObstacle = false;
	private float obstacleDelayTime = 0.0f;
	private float targetMovementRestoreTime = 1.0f;
	private PlayerAnimState prevAnimState;
	private PlayerAnimState currentAnimState;
	private PackedSprite _packedSprite;

	// Use this for initialization
	void Start () {
		_packedSprite = GetComponent<PackedSprite>();
		prevAnimState = PlayerAnimState.IDLE;
		currentAnimState = PlayerAnimState.IDLE;
		hairStrandSwitchTime = 0.0f;
		switchingHairStrand = false;
		gotHitByObstacle = false;
		obstacleDelayTime = 0.0f;

		Vector3 currentPosition = transform.position;
		currentPosition.x = hairStrands[currentHairStrandIndex].transform.position.x;
		transform.position = currentPosition;
		prevHairStrandIndex = currentHairStrandIndex;
		canMove = true;
		isDying = false;
		hasReachedGoal = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(!hasReachedGoal && !isDying) {
			if(!gotHitByObstacle) {
				if(canMove) {
					float horizontal = Input.GetAxis ("Horizontal");
					//float vertical = Input.GetAxis("Vertical");
					/*
					float vertical = 1.0f;
					if(vertical == 0.0f) {
						currentAnimState = PlayerAnimState.IDLE;
						if(currentAnimState != prevAnimState) {
							_packedSprite.PlayAnim("idle");
							prevAnimState = currentAnimState;
						}
					} else {
						currentAnimState = PlayerAnimState.CLIMB;
						if(currentAnimState != prevAnimState) {
							_packedSprite.PlayAnim("climb");
							prevAnimState = currentAnimState;
						}
					}
					verticalClimb(vertical);
					*/

					if(horizontal < 0) {
						changeHairStrandToLeft();
					} else if(horizontal > 0) {
						changeHairStrandToRight();
					}
				}
				float vertical = 1.0f;
				if(vertical == 0.0f) {
					currentAnimState = PlayerAnimState.IDLE;
					if(currentAnimState != prevAnimState) {
						_packedSprite.PlayAnim("idle");
						prevAnimState = currentAnimState;
					}
				} else {
					currentAnimState = PlayerAnimState.CLIMB;
					if(currentAnimState != prevAnimState) {
						_packedSprite.PlayAnim("climb");
						prevAnimState = currentAnimState;
					}
				}
				verticalClimb(vertical);
				
				//updateHorizontalMovement();
			} else {
				if(obstacleDelayTime >= targetMovementRestoreTime) {
					gotHitByObstacle = false;
					renderer.enabled = true;
					obstacleDelayTime = 0.0f;
				} else {
					renderer.enabled = !renderer.enabled;
					obstacleDelayTime += Time.deltaTime;
				}
			}

			updateHorizontalMovement();
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		if(other.gameObject.tag == "Goal") {
			if(!hasReachedGoal) {
				currentAnimState = PlayerAnimState.IDLE;
				if(currentAnimState != prevAnimState) {
					_packedSprite.PlayAnim("idle");
					prevAnimState = currentAnimState;
				}
				rigidbody2D.velocity = new Vector2(0.0f, 0.0f);
				canMove = false;
				hasReachedGoal = true;
				GameObject.FindGameObjectWithTag("Dragon").GetComponent<DragonController>().Stop();
			}
		}
	}

	void OnCollisionEnter2D(Collision2D collision) {
		if(collision.gameObject.tag == "Dragon") {
			if(!isDying) {
				currentAnimState = PlayerAnimState.IDLE;
				if(currentAnimState != prevAnimState) {
					_packedSprite.PlayAnim("idle");
					prevAnimState = currentAnimState;
				}
				rigidbody2D.velocity = new Vector2(0.0f, 0.0f);
				canMove = false;
				renderer.enabled = false;
				StartCoroutine("Die");
			}
		}
	}

	void updateHorizontalMovement() {
		if(switchingHairStrand) {
			//Debug.Log("Switching hair strand");
			if(hairStrandSwitchTime >= 0.5f) {
				//Debug.Log ("Switching stops.");
				hairStrandSwitchTime = 0.0f;
				switchingHairStrand = false;
			} else {
				//Debug.Log ("Still switching...");
				float distanceInterval = hairStrands[currentHairStrandIndex].transform.position.x - hairStrands[prevHairStrandIndex].transform.position.x;
				//rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, 0.0f);
				Vector3 currentPosition = transform.position;
				currentPosition.x += distanceInterval * (Time.deltaTime * 2);
				transform.position = currentPosition;
				hairStrandSwitchTime += Time.deltaTime;
			}
		}
	}

	void verticalClimb(float yAxis) {
		rigidbody2D.AddForce(new Vector2(0.0f, yAxis * speed));

		if(Mathf.Abs(rigidbody2D.velocity.y) > maxSpeed) {
			rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, Mathf.Sign(rigidbody2D.velocity.y) * maxSpeed);
		}
	}

	void changeHairStrandToLeft() {
		if(currentHairStrandIndex > 0) {
			currentHairStrandIndex--;
			StartCoroutine("ChangeToAnotherHairStrand");
		}
	}

	void changeHairStrandToRight() {
		if(currentHairStrandIndex < (hairStrands.Count - 1)) {
			currentHairStrandIndex++;
			StartCoroutine("ChangeToAnotherHairStrand");
		}
	}

	public void StopPlayer(float numOfSeconds) {
		targetMovementRestoreTime = numOfSeconds;
		gotHitByObstacle = true;
		obstacleDelayTime = 0.0f;
		rigidbody2D.velocity = new Vector2(0.0f, 0.0f);
		currentAnimState = PlayerAnimState.IDLE;
		if(currentAnimState != prevAnimState) {
			_packedSprite.PlayAnim("idle");
			prevAnimState = currentAnimState;
		}
	}
	
	IEnumerator ChangeToAnotherHairStrand() {
		canMove = false;
		switchingHairStrand = true;
		//rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, 0.0f);
		yield return new WaitForSeconds(0.5f);
		prevHairStrandIndex = currentHairStrandIndex;
		Vector3 currentPosition = transform.position;
		currentPosition.x = hairStrands[currentHairStrandIndex].transform.position.x;
		transform.position = currentPosition;
		canMove = true;
	}

	IEnumerator Die() {
		isDying = true;
		yield return new WaitForSeconds(3.0f);
		Application.LoadLevel("Lose");
	}
}
