﻿using UnityEngine;
using System.Collections;

public class DragonController : MonoBehaviour {
	public float speed = 30.0f;
	public float maxSpeed = 30.0f;
	public GameObject playerObject;

	private bool isMoving = true;

	// Use this for initialization
	void Start () {
		playerObject = GameObject.FindGameObjectWithTag("Player");
		isMoving = true;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 currentPosition = transform.position;
		currentPosition.x = playerObject.transform.position.x;

		if(isMoving) {
			rigidbody2D.AddForce(new Vector2(0.0f, speed));

			if(Mathf.Abs(rigidbody2D.velocity.y) > maxSpeed) {
				rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, Mathf.Sign(rigidbody2D.velocity.y) * maxSpeed);
			}
			//currentPosition.y += rigidbody2D.velocity.y;
		} else {
			rigidbody2D.velocity = new Vector2(0.0f, 0.0f);
		}

		transform.position = currentPosition;
	}

	void OnCollisionEnter2D(Collision2D collision) {
		if(collision.gameObject.tag == "Player") {
			if(audio) {
				audio.Play ();
			}
			isMoving = false;
		}
	}

	public void Stop() {
		isMoving = false;
	}
}
