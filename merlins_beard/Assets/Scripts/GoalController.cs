﻿using UnityEngine;
using System.Collections;

public class GoalController : MonoBehaviour {
	private bool goalTouched = false;

	// Use this for initialization
	void Start () {
		goalTouched = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter2D(Collider2D other) {
		if(other.gameObject.tag == "Player") {
			if(!goalTouched) {
				GameObject.FindGameObjectWithTag("MainCamera").GetComponent<MainCameraController>().InitiateEndingSequence();
				goalTouched = true;
				StartCoroutine("WaitTillWinScene");
			}
		}
	}

	IEnumerator WaitTillWinScene() {
		yield return new WaitForSeconds(4.0f);
		Application.LoadLevel ("Win");
	}
}
