﻿using UnityEngine;
using System.Collections;

public class SceneTransitionKeyController : MonoBehaviour {
	public string sceneName;
	public GameObject fadeObject;

	private bool canInteract = false;
	private bool isFading = false;
	private bool isFadingIn = true;

	// Use this for initialization
	void Start () {
		canInteract = false;
		isFading = false;
		isFadingIn = false;
		if(!fadeObject) {
			canInteract = true;
		} else {
			isFading = true;
			isFadingIn = true;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(canInteract) {
			if(Input.anyKey) {
				isFadingIn = false;
				isFading = true;
				canInteract = false;
			}
		}

		if(isFading) {
			Color currentFadeAlpha = fadeObject.renderer.material.color;
			if(isFadingIn) {
				if(fadeObject.renderer.material.color.a <= 0.0f) {
					currentFadeAlpha.a = 0.0f;
					fadeObject.renderer.material.color = currentFadeAlpha;
					isFadingIn = false;
					isFading = false;
					canInteract = true;
				} else {
					currentFadeAlpha.a -= Time.deltaTime;
					fadeObject.renderer.material.color = currentFadeAlpha;
				}
			} else {
				if(fadeObject.renderer.material.color.a >= 1.0f) {
					currentFadeAlpha.a = 1.0f;
					fadeObject.renderer.material.color = currentFadeAlpha;
					Application.LoadLevel(sceneName);
				} else {
					currentFadeAlpha.a += Time.deltaTime;
					fadeObject.renderer.material.color = currentFadeAlpha;
				}
			}
		}
	}
}
