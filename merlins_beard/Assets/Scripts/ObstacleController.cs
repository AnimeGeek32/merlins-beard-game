﻿using UnityEngine;
using System.Collections;

public class ObstacleController : MonoBehaviour {
	public float speed = 30.0f;
	public float maxSpeed = 30.0f;
	public float playerDelayTime = 1.0f;
	public float deathByHeight = -100.0f;
	public AudioClip playerHitSound;

	protected PackedSprite packedSprite;

	// Use this for initialization
	void Start () {
		packedSprite = GetComponent<PackedSprite>();
		if(packedSprite) {
			packedSprite.PlayAnim("falling");
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(transform.position.y <= deathByHeight) {
			Destroy (gameObject);
		}
		rigidbody2D.AddForce(new Vector2(0.0f, -speed));

		if(Mathf.Abs(rigidbody2D.velocity.y) > maxSpeed) {
			rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, Mathf.Sign(rigidbody2D.velocity.y) * maxSpeed);
		}
	}

	/*
	void OnCollisionEnter2D(Collision2D collision) {
		if(collision.gameObject.tag == "Player") {
			Debug.Log ("Obstacle slows down player.");
			collision.gameObject.GetComponent<PlayerController>().StopPlayer(playerDelayTime);
			Destroy (gameObject);
		} else if(collision.gameObject.tag == "Dragon") {
			Destroy (gameObject);
		}
	}
	*/

	void OnTriggerEnter2D(Collider2D other) {
		if(other.gameObject.tag == "Player") {
			if(playerHitSound) {
				other.gameObject.audio.PlayOneShot(playerHitSound);
			}
			other.gameObject.GetComponent<PlayerController>().StopPlayer(playerDelayTime);
			Destroy (gameObject);
		} else if(other.gameObject.tag == "Dragon") {
			Destroy (gameObject);
		}
	}
}
